package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {
    private Context context;
    private List<Workout> workoutList;

    public HistoryListAdapter(Context context, int resource, ArrayList<Workout> objects) {
        super(context, resource, objects);

        this.context = context;
        this.workoutList = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        //get the property we are displaying
        Workout workout = workoutList.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_history, null);

        TextView title = (TextView) view.findViewById(R.id.textview_history_title);
        TextView date = (TextView) view.findViewById(R.id.textview_history_datetime);
        TextView opis = (TextView) view.findViewById(R.id.textview_history_sportactivity);
        ImageView image = (ImageView) view.findViewById(R.id.imageview_history_icon);

        String sport = "";
        if (workout.getSportActivity() == 0) {
            sport = "Running";
        } else if (workout.getSportActivity() == 1) {
            sport = "Walking";
        } else {
            sport = "Cycling";
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        String titleString = workout.getTitle();
        String dateString = dateFormat.format(workout.getCreated());

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String unit = sharedPref.getString("unit", "Kilometers");

        double distance = workout.getDistance();
        double avgPace = workout.getPaceAvg();

        String opisString = "";
        if (unit.equals("Kilometers")) {
            opisString = MainHelper.formatDuration2(workout.getDuration()) + " " + sport + " | " +
                    MainHelper.formatDistance(distance) + " km" + " | " +
                    MainHelper.formatCalories(workout.getTotalCalories()) + " kcal" + " | " +
                    MainHelper.formatPace(avgPace) + " min/km";
        } else if (unit.equals("Miles")) {
            distance = MainHelper.kmToMi(distance);
            avgPace = MainHelper.minpkmToMinpmi(avgPace);
            opisString = MainHelper.formatDuration2(workout.getDuration()) + " " + sport + " | " +
                    MainHelper.formatDistance(distance) + " mi" + " | " +
                    MainHelper.formatCalories(workout.getTotalCalories()) + " kcal" + " | " +
                    MainHelper.formatPace(avgPace) + " min/mi";
        }




        title.setText(titleString);
        date.setText(dateString);
        opis.setText(opisString);
        image.setImageResource(R.drawable.ic_launcher_background);

        return view;
    }

}
