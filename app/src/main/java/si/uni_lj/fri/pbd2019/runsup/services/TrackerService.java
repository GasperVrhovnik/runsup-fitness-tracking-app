package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

import si.uni_lj.fri.pbd2019.runsup.StopwatchActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;


public class TrackerService extends Service {


    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public TrackerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackerService.this;
        }
    }


    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    public final String startCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public final String contCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public final String pauseCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public final String stopCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public final String updateActivity = "si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY";
    public final Integer STATE_RUNNING = 0;
    public final Integer STATE_STOPPED = 1;
    public final Integer STATE_PAUSED = 2;
    public final Integer STATE_CONTINUE = 3;

    private Integer state;
    private int noLocationUpdates;

    private long duration;
    private double distance;
    private double pace;
    private double calories;
    private Integer sportActivity;
    private ArrayList<Location> positionList;
    private Location lastLocation;
    private List<Float> speedList;
    private int sessionNumber;

    private long start;
    private double distanceCompare;

    private long workoutId;

//    private Timer timer;
    private Handler mHandler = new Handler();

    public double getDistance() {
        return distance;
    }

    public Integer getState() {
        return state;
    }

    public long getDuration() {
        return duration;
    }

    public double getPace() {
        return pace;
    }




    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("test", "in onCreate");
        mFusedLocationProviderClient = new FusedLocationProviderClient(this);
        noLocationUpdates = 0;

        state = STATE_STOPPED;
        getLocations();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("test", "in OnStartCommand");


        String action = intent.getAction() == null ? "" : intent.getAction();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (state.equals(STATE_RUNNING) || state.equals(STATE_CONTINUE)) {



                    Log.i("service", "run method");
                    duration = (SystemClock.uptimeMillis() - start);
                    //start = SystemClock.uptimeMillis();
                    if (distance == distanceCompare) {
                        noLocationUpdates++;
                    } else {
                        distanceCompare = distance;
                        noLocationUpdates = 0;
                    }

                    if (noLocationUpdates >= 6) {
                        pace = 0.0;
                    }
                    Intent i = new Intent("si.uni_lj.fri.pbd2019.runsup.TICK");
                    i.putExtra("distance", distance);
                    i.putExtra("duration", duration / 1000);
                    i.putExtra("pace", pace);
                    i.putExtra("calories", calories);
                    i.putExtra("state", state);
                    i.putExtra("sportActivity", sportActivity);
                    i.putExtra("lastLocation", lastLocation);
                    i.putExtra("state", state);
                    i.putExtra("sessionNumber", sessionNumber);
                    LocalBroadcastManager.getInstance(TrackerService.this).sendBroadcast(i);
                    mHandler.postDelayed(this, 1000);
                }
            }
        };

        switch (action) {
            case startCmd:
                workoutId = intent.getLongExtra("workoutId", 0);
                Log.i("service", intent.getAction());
                duration = 0;
                distance = 0;
                distanceCompare = 0;
                pace = 0;
                calories = 0;
                sessionNumber = 0;
                state = STATE_RUNNING;
                start = SystemClock.uptimeMillis();
                runnable.run();
                break;
            case contCmd:
                Log.i("service", intent.getAction());
                start = SystemClock.uptimeMillis() - duration;
                state = STATE_CONTINUE;
                runnable.run();
                break;
            case pauseCmd:
                Log.i("service", intent.getAction());
                //duration += (SystemClock.uptimeMillis() - start);
                mHandler.removeCallbacks(runnable);
                state = STATE_PAUSED;
                sessionNumber++;
                break;
            case stopCmd:
                Log.i("service", intent.getAction());
                //duration += (SystemClock.uptimeMillis() - start);
                mHandler.removeCallbacks(runnable);
                state = STATE_STOPPED;
                break;
            case updateActivity:
                sportActivity = intent.getExtras().getInt("sportActivity",0);
                break;
        }




        return START_STICKY;
    }

    public void getLocations() {
        positionList = new ArrayList<Location>();
        speedList = new LinkedList<Float>();



        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setSmallestDisplacement(10);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (positionList.size() > 0 && (state == STATE_CONTINUE || state == STATE_RUNNING) && positionList.get(positionList.size()-1).distanceTo(locationResult.getLastLocation()) >= 2 ) {
                    noLocationUpdates = 0;
                    lastLocation = locationResult.getLastLocation();
                    positionList.add(locationResult.getLastLocation());
                    calculateDistance();
                    calculatePace();
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    if (sharedPref.contains("weight")) {
                        calories = SportActivities.countCalories(0, sharedPref.getInt("weight", 60), speedList, ((double)duration/1000.0) / 3600.0);
                    } else {
                        calories = SportActivities.countCalories(0, 60, speedList, ((double)duration/1000.0) / 3600.0);
                    }
                    Log.i("test", "Distance: " + distance);
                    Log.i("test", "Calories: " + calories);
                } else if (positionList.size() == 0 && (state == STATE_CONTINUE || state == STATE_RUNNING)){
                    positionList.add(locationResult.getLastLocation());
                    lastLocation = locationResult.getLastLocation();
                    noLocationUpdates = 0;
                }
                DatabaseHelper databaseHelper = OpenHelperManager.getHelper(TrackerService.this, DatabaseHelper.class);
                try {
                    if (lastLocation != null && (state == STATE_CONTINUE || state == STATE_RUNNING)) {
                        Log.i("GPSlist", "lokacija tukaj");
                        Dao<GpsPoint, Long> gpsDao = databaseHelper.gpsPointDao();
                        GpsPoint point = new GpsPoint();
                        point.setCreated(new Date());
                        point.setDuration(duration);
                        point.setLatitude(lastLocation.getLatitude());
                        point.setLongitude(lastLocation.getLongitude());
                        point.setPace(pace);
                        point.setSessionNumber(sessionNumber);
                        point.setWorkout(databaseHelper.workoutDao().queryForId(workoutId));
                        point.setTotalCalories(calories);
                        Log.i("point", "sessNum: " + point.getSessionNumber());
                        gpsDao.create(point);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        };


        if(!(ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        }
    }

    public void calculateDistance() {
        if (positionList.size() <= 1) {
            distance = 0;
        } else {
            double lastDist = positionList.get(positionList.size()-1).distanceTo(positionList.get(positionList.size()-2));
            distance += lastDist;
            speedList.add((float)lastDist / 3); //get location updates every 3 seconds
        }
    }

    public void calculatePace() {
        double minutes = ((double)duration/1000.0) / 60.0;
        double km = distance / 1000.0;
        pace = minutes / km;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}
