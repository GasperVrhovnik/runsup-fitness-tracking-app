package si.uni_lj.fri.pbd2019.runsup;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ArrayList<List<Location>> positionList;
    private Integer sportActivity;
    private long duration;
    private double distance;
    private double pace;
    private double calories;

    private final Integer RUNNING = 0;
    private final Integer WALKING = 1;
    private final Integer CYCLING = 2;

    private TextView tvWorkoutTitle;
    private TextView tvSportActivity;
    private TextView tvActivityDate;
    private TextView tvDuration;
    private TextView tvCalories;
    private TextView tvDistance;
    private TextView tvAvgPace;

    private Long workoutId;

    private Button email;
    private Button facebook;
    private Button twitter;

    private DatabaseHelper databaseHelper;

    private Workout workout;
    private GoogleMap mMap;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_workoutdetail_map);
        mapFragment.getMapAsync(this);

        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);

        email = findViewById(R.id.button_workoutdetail_emailshare);
        facebook = findViewById(R.id.button_workoutdetail_fbsharebtn);
        twitter = findViewById(R.id.button_workoutdetail_twittershare);

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "lalala@gmail.com"});
                email.putExtra(Intent.EXTRA_SUBJECT, "Look at my workout");
                email.putExtra(Intent.EXTRA_TEXT, "I was out for " + sportActivity + "I did " + distance + " km in " + duration + ".");

                //need this to prompts email client only
                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, "Choose an Email client :"));

            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "text to be shared");
                PackageManager pm = getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.name).contains("facebook")) {
                        final ActivityInfo activity = app.activityInfo;
                        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        shareIntent.setComponent(name);
                        startActivity(shareIntent);
                        break;
                    }
                }

            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tweet = new Intent(Intent.ACTION_VIEW);
                tweet.setData(Uri.parse("http://twitter.com/?status=" + Uri.encode("I was out for " + sportActivity + "I did " + distance + " km in " + duration + ".")));//where message is your string message
                startActivity(tweet);
            }
        });

        tvWorkoutTitle = findViewById(R.id.textview_workoutdetail_workouttitle);
        tvSportActivity = findViewById(R.id.textview_workoutdetail_sportactivity);
        tvActivityDate = findViewById(R.id.textview_workoutdetail_activitydate);
        tvDuration = findViewById(R.id.textview_workoutdetail_valueduration);
        tvCalories = findViewById(R.id.textview_workoutdetail_valuecalories);
        tvDistance = findViewById(R.id.textview_workoutdetail_valuedistance);
        tvAvgPace = findViewById(R.id.textview_workoutdetail_valueavgpace);

        workoutId = getIntent().getLongExtra("workoutId", 0);

        try {
            Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
            workout = workoutLongDao.queryForId(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tvWorkoutTitle.setText(workout.getTitle());
        if (workout.getSportActivity() == 0) {
            tvSportActivity.setText("Running");
        } else if (workout.getSportActivity() == 1) {
            tvSportActivity.setText("Walking");
        } else {
            tvSportActivity.setText("Cycling");
        }

        double distance = workout.getDistance();
        double avgPace = workout.getPaceAvg();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String unit = sharedPref.getString("unit", "Kilometers");

        if (unit.equals("Kilometers")) {
            tvDistance.setText(MainHelper.formatDistance(distance) + " km");
            tvAvgPace.setText(MainHelper.formatPace(avgPace) + " min/km");
        } else if (unit.equals("Miles")) {
            distance = MainHelper.kmToMi(distance);
            tvDistance.setText(MainHelper.formatDistance(distance) + " mi");
            avgPace = MainHelper.minpkmToMinpmi(avgPace);
            tvAvgPace.setText(MainHelper.formatPace(avgPace) + " min/mi");
        }

        tvDuration.setText(MainHelper.formatDuration(workout.getDuration()));
        tvCalories.setText(MainHelper.formatCalories(workout.getTotalCalories()) + " kcal");
        tvActivityDate.setText(SimpleDateFormat.getTimeInstance().format(new Date()));



        tvWorkoutTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(WorkoutDetailActivity.this);
                builder.setTitle("Change workout title");
                builder.setMessage("Input new workout title:");


                final EditText input = new EditText(WorkoutDetailActivity.this);

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = input.getText().toString();
                        tvWorkoutTitle.setText(title);
                        workout.setTitle(title);
                        try {
                            databaseHelper.workoutDao().update(workout);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setAllGesturesEnabled(false);


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent i = new Intent(WorkoutDetailActivity.this, MapsActivity.class);
                i.putExtra("workoutId", workoutId);
                startActivity(i);
            }
        });

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                try {
                    Dao<GpsPoint,Long> gpsDao = databaseHelper.gpsPointDao();
                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

                    List<GpsPoint> gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", 0).query();
                    boolean vsajEnPoint = gpsList.size() > 0;

                    if (vsajEnPoint) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude()))).setTitle("Start");


                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                            boundsBuilder.include(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                    }
                    GpsPoint last = null;
                    if (vsajEnPoint) {
                        last = gpsList.get(gpsList.size() - 1);
                    }
                    int i = 1;
                    while(gpsList.size() > 0) {
                        gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", i).query();
                        if (gpsList.size() <= 0) {
                            break;
                        }
                        Location lastL = new Location(LocationManager.GPS_PROVIDER);
                        lastL.setLatitude(last.getLatitude());
                        lastL.setLongitude(last.getLongitude());
                        Location firstL = new Location(LocationManager.GPS_PROVIDER);
                        firstL.setLatitude(gpsList.get(0).getLatitude());
                        firstL.setLongitude(gpsList.get(0).getLongitude());
                        if (lastL.distanceTo(firstL) > 100) {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("Pause  " + i)).showInfoWindow(); //last of lower sess number
                            mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude())).title("Continue  " + i)).showInfoWindow(); //first
                        } else {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("Break  " + i)).showInfoWindow();
                        }

                        last = gpsList.get(gpsList.size() - 1);
                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                            boundsBuilder.include(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                        i++;
                    }
                    if (vsajEnPoint) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("End")).showInfoWindow();
                    }


                    if (vsajEnPoint) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));
                    }



                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(getResources().getString(R.string.all_actiondelete));
        menu.add(getResources().getString(R.string.all_actionsettings));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        CharSequence id = item.getTitle();
        if (id == getResources().getString(R.string.all_actionsettings)) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == getResources().getString(R.string.all_actiondelete)) {
            try {
                Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
                workout.setStatus(Workout.statusDeleted);
                workoutLongDao.update(workout);
                finish();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
