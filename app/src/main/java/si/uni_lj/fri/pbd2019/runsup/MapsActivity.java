package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Long workoutId;
    private Workout workout;
    private DatabaseHelper databaseHelper;
    private Dao<Workout, Long> workoutDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_map);
        mapFragment.getMapAsync(this);

        Intent i = getIntent();
        workoutId = i.getLongExtra("workoutId", 0);

        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
            workout = workoutDao.queryForId(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                try {
                    Dao<GpsPoint,Long> gpsDao = databaseHelper.gpsPointDao();
                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

                    List<GpsPoint> gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", 0).query();
                    boolean vsajEnPoint = gpsList.size() > 0;

                    if (vsajEnPoint) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude()))).setTitle("Start");


                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                            boundsBuilder.include(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                    }
                    GpsPoint last = null;
                    if (vsajEnPoint) {
                        last = gpsList.get(gpsList.size() - 1);
                    }

                    int i = 1;
                    while(gpsList.size() > 0) {
                        gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", i).query();
                        if (gpsList.size() <= 0) {
                            break;
                        }
                        Location lastL = new Location(LocationManager.GPS_PROVIDER);
                        lastL.setLatitude(last.getLatitude());
                        lastL.setLongitude(last.getLongitude());
                        Location firstL = new Location(LocationManager.GPS_PROVIDER);
                        firstL.setLatitude(gpsList.get(0).getLatitude());
                        firstL.setLongitude(gpsList.get(0).getLongitude());
                        if (lastL.distanceTo(firstL) > 100) {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("Pause  " + i)).showInfoWindow(); //last of lower sess number
                            mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude())).title("Continue  " + i)).showInfoWindow(); //first
                        } else {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("Break  " + i)).showInfoWindow();
                        }

                        last = gpsList.get(gpsList.size() - 1);
                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                            boundsBuilder.include(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                        i++;
                    }

                    if (vsajEnPoint) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("End")).showInfoWindow();
                    }


                    if (vsajEnPoint) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));
                    }


                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
