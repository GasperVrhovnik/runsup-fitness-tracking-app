package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.HashMap;
import java.util.List;

public final class SportActivities {

    public final static int RUNNING = 0;
    public final static int WALKING = 1;
    public final static int CYCLING = 2;
    public final static Integer[] activities = {0, 1, 2};
    /**
     * Returns MET value for an activity.
     * @param activityType - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param speed - speed in m/s
     * @return
     */
    public static double getMET(int activityType, Float speed) {

        speed = (float) MainHelper.mpsToMiph(speed);
        Double run = 1.535353535;
        Double walk = 1.14;
        Double cycle = 0.744444444;

        HashMap<Integer, Double> running = new HashMap<Integer, Double>();
        running.put(4, 6.0);
        running.put(5, 8.3);
        running.put(6, 9.8);
        running.put(7, 11.0);
        running.put(8, 11.8);
        running.put(9, 12.8);
        running.put(10, 14.5);
        running.put(11, 16.0);
        running.put(12, 19.0);
        running.put(13, 19.8);
        running.put(14, 23.0);

        HashMap<Integer, Double> walking = new HashMap<Integer, Double>();
        walking.put(1, 2.0);
        walking.put(2, 2.8);
        walking.put(3, 3.1);
        walking.put(4, 3.5);

        HashMap<Integer, Double> cycling = new HashMap<Integer, Double>();
        cycling.put(10, 6.8);
        cycling.put(12, 8.0);
        cycling.put(14, 10.0);
        cycling.put(16, 12.8);
        cycling.put(18, 13.6);
        cycling.put(20, 15.8);


        if (activityType == 0) { // running
            if (running.containsKey(Math.ceil(speed))) {
                return running.get(Math.ceil(speed));
            } else {
                return run * speed;
            }
        } else if (activityType == 1) { // walking
            if (walking.containsKey(Math.ceil(speed))) {
                return walking.get(Math.ceil(speed));
            } else {
                return walk * speed;
            }
        } else { // cycling
            if (cycling.containsKey(Math.ceil(speed))) {
                return cycling.get(Math.ceil(speed));
            } else {
                return cycle * speed;
            }
        }
    }

    /**
     * Returns final calories computed from the data provided (returns value in kcal)
     * @param sportActivity - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight - weight in kg
     * @param speedList - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
     * @return
     */
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours) {
        float avgSpeed = 0;
        int i = 0;
        for (float speed : speedList) {
            avgSpeed += speed;
            i++;
        }
        avgSpeed = avgSpeed / i;

        Log.i("test", "getMETS: " + Double.toString(getMET(sportActivity, avgSpeed)));
        Log.i("test", "Time: " + timeFillingSpeedListInHours);

        return  getMET(sportActivity, avgSpeed) * weight * timeFillingSpeedListInHours;
    }

    public static String getSportActivityName(int i) {
        if (i == 0) {
            return "Running";
        } else if (i == 1) {
            return "Walking";
        } else {
            return "Cycling";
        }
    }



}
