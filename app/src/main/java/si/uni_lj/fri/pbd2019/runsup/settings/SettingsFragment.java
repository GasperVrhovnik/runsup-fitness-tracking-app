package si.uni_lj.fri.pbd2019.runsup.settings;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsApi;

import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    private int locationRequestCode = 1000;
    private CheckBoxPreference gps;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pref_main, rootKey);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());

        gps = (CheckBoxPreference) findPreference("gps");
        final ListPreference unit = (ListPreference) findPreference("unit");
        unit.setSummary(sharedPref.getString("unit", "Kilometers"));

        unit.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String val = (String) newValue;
                if (val.equals("Kilometers")) {
                    unit.setSummary("Kilometers");

                } else {
                    unit.setSummary("Miles");
                }
                return true;
            }
        });

        LocationManager lm = (LocationManager)getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!gps_enabled) {
            //ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
            gps.setChecked(false);
        } else {
            gps.setChecked(true);
        }

        gps.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Boolean val = (Boolean) newValue;
                if (val) {
                    new AlertDialog.Builder(getContext())
                            .setMessage("Please enable Location sensing")
                            .setPositiveButton("open settings", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    getContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton(R.string.cancel,null)
                            .show();

                    return true;
                } else {

                    new AlertDialog.Builder(getContext())
                            .setMessage("Please disable Location sensing")
                            .setPositiveButton("open settings", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    getContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton(R.string.cancel,null)
                            .show();

                    return true;
                }
            }
        });



        Log.i("unit", "UNIT: " + sharedPref.getString("unit", "Kilometers"));

    }







}
