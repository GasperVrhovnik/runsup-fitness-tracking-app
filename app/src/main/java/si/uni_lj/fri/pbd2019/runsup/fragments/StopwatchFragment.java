package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;


import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.MainActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.StopwatchActivity;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StopwatchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StopwatchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StopwatchFragment extends Fragment {

    public final String startCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public final String contCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public final String pauseCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public final String stopCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public final String updateActivity = "si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY";
    private final Integer RUNNING = 0;
    private final Integer WALKING = 1;
    private final Integer CYCLING = 2;
    public static final int STATE_RUNNING = 0;
    public static final int STATE_STOPPED = 1;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_CONTINUE = 3;

    private int state;


    private BroadcastReceiver mBroadcastReceiver;
    private int locationRequestCode = 1000;

    private Button start;
    private Button endWorkout;
    private Button btnSportActivity;

    private TextView distanceText;
    private TextView durationText;
    private TextView paceText;
    private TextView caloriesText;
    private TextView unitDistance;
    private TextView unitPace;

    private ArrayList<List<Location>> positionList;
    private LinkedList<Location> currPositionList;
    private Integer sportActivity;
    private long duration;
    private double distance;
    private double pace;
    private double calories;
    private Location lastLocation;
    private int sessionNumber;

    private boolean workout;
    private long workoutId;
    private Workout w;
    private boolean permissionsNotGranted;

    private DatabaseHelper databaseHelper;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public StopwatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StopwatchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StopwatchFragment newInstance(String param1, String param2) {
        StopwatchFragment fragment = new StopwatchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        databaseHelper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
        permissionsNotGranted = ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        if (permissionsNotGranted) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_stopwatch, container, false);

        distanceText = (TextView) view.findViewById(R.id.textview_stopwatch_distance);
        durationText = (TextView) view.findViewById(R.id.textview_stopwatch_duration);
        paceText = (TextView) view.findViewById(R.id.textview_stopwatch_pace);
        caloriesText = (TextView) view.findViewById(R.id.textview_stopwatch_calories);
        start = (Button) view.findViewById(R.id.button_stopwatch_start);
        endWorkout = (Button) view.findViewById(R.id.button_stopwatch_endworkout);
        unitDistance = view.findViewById(R.id.textview_stopwatch_distanceunit);
        unitPace = view.findViewById(R.id.textview_stopwatch_unitpace);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String unit = sharedPref.getString("unit", "Kilometers");

        if (unit.equals("Kilometers")) {
            unitDistance.setText(getResources().getString(R.string.km));
            unitPace.setText("min/km");
        } else if (unit.equals("Miles")) {
            unitDistance.setText(getResources().getString(R.string.mi));
            unitPace.setText("min/mi");
        }

        positionList = new ArrayList<List<Location>>();
        sportActivity = RUNNING;
        duration = 0;
        distance = 0.0;
        pace = 0.0;
        state = STATE_STOPPED;

        workout = false;
        w = new Workout();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (start.getText().toString().equals(getResources().getString(R.string.stopwatch_start))) {
                    workout = true;
                    state = STATE_RUNNING;
                } else if (start.getText().toString().equals(getResources().getString(R.string.stopwatch_stop))) {
                    state = STATE_PAUSED;
                } else {
                    state = STATE_CONTINUE;
                }

                Intent i = new Intent(getActivity(), TrackerService.class);
                switch (state) {
                    case STATE_RUNNING:
                        sessionNumber = 0;
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        i.setAction(startCmd);
                        w.setCreated(new Date());
                        w.setSportActivity(sportActivity);
                        w.setStatus(Workout.statusUnknown);
                        try {
                            Dao<Workout, Long> workoutDao =  databaseHelper.workoutDao();
                            workoutDao.create(w);
                            w.setTitle("Workout " + Long.toString(w.getId()));
                            w.setStatus(Workout.statusUnknown);
                            workoutId = w.getId();
                            workoutDao.update(w);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        i.putExtra("workoutId", workoutId);
                        i.putExtra("sportActivity", sportActivity);
                        break;
                    case STATE_CONTINUE:
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        endWorkout.setVisibility(View.GONE);
                        w.setStatus(Workout.statusUnknown);
                        sessionNumber++;
                        try {
                            Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
                            workoutLongDao.update(w);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        i.setAction(contCmd);
                        break;
                    case STATE_PAUSED:
                        start.setText(getResources().getString(R.string.stopwatch_continue));
                        endWorkout.setVisibility(View.VISIBLE);
                        //positionList.add(currPositionList); //dodaj novo pot po pauzi
                        //currPositionList = new LinkedList<Location>(); //nastavi prazno pot za morebiten continue
                        w.setStatus(Workout.statusPaused);
                        try {
                            Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
                            workoutLongDao.update(w);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        i.setAction(pauseCmd);
                        break;
                    case STATE_STOPPED:
                        i.setAction(stopCmd);
                        break;
                }
                Log.i("service", i.getAction());
                getActivity().startService(i);


            }
        });





        endWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(StopwatchActivity.this, WorkoutDetailActivity.class);
//                startActivity(intent);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Do you really want to end workout and reset counters?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                workout = false;
                                state = STATE_STOPPED;
                                w.setStatus(Workout.statusEnded);
                                try {
                                    Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
                                    workoutLongDao.update(w);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(getContext(), WorkoutDetailActivity.class);
                                intent.putExtra("workoutId", workoutId);
                                pace = ((double) duration / 60.0) / (distance / 1000.0);
                                start.setText("Start");
                                endWorkout.setVisibility(View.GONE);
                                intent.putExtra("pace", pace);
                                startActivity(intent);
                            }
                        });

                builder1.setNegativeButton(
                        getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                distance = intent.getExtras().getDouble("distance");
                duration = intent.getExtras().getLong("duration");
                calories = intent.getExtras().getDouble("calories");
                lastLocation = (Location) intent.getExtras().get("lastLocation");
                sessionNumber = intent.getIntExtra("sessionNumber", 0);
                pace = intent.getExtras().getDouble("pace");
                sportActivity = intent.getExtras().getInt("sportActivity");
//                ArrayList<Location> currPositionArray = (ArrayList<Location>) intent.getExtras().get("positionList");
//                currPositionList = new LinkedList<Location>();
//                for (Location l : currPositionArray) {
//                    currPositionList.add(l);
//                }
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
                String unit = sharedPref.getString("unit", "Kilometers");

                if (unit.equals("Kilometers")) {
                    unitDistance.setText(getResources().getString(R.string.km));
                    unitPace.setText("min/km");             //ni potrebe po pretvorbi
                } else if (unit.equals("Miles")) {
                    unitDistance.setText(getResources().getString(R.string.mi));
                    unitPace.setText("min/mi");
                    distance = MainHelper.kmToMi(distance); //pretvori
                    pace = MainHelper.minpkmToMinpmi(pace);
                }

                String distanceString = MainHelper.formatDistance(distance);
                String durationString = MainHelper.formatDuration(duration);
                String paceString = MainHelper.formatPace(pace);
                String caloriesString = MainHelper.formatCalories(calories);

                distanceText.setText(distanceString);
                durationText.setText(durationString);
                paceText.setText(paceString);
                caloriesText.setText(caloriesString);
                state = intent.getIntExtra("state", 0);
                switch (state) {
                    case STATE_RUNNING:
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        break;
                    case STATE_CONTINUE:
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        endWorkout.setVisibility(View.GONE);
                        break;
                    case STATE_PAUSED:
                        start.setText(getResources().getString(R.string.stopwatch_continue));
                        endWorkout.setVisibility(View.VISIBLE);
                        break;
                    case STATE_STOPPED:
                        break;
                }

                DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class);
                try {
                    Dao<Workout, Long> workoutDao =  databaseHelper.workoutDao();
                    w.setDistance(distance);
                    w.setDuration(duration);
                    w.setTotalCalories(calories);
                    w.setPaceAvg((duration / 60) / (distance / 1000));
                    w.setSportActivity(sportActivity);
                    workoutDao.update(w);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Log.i("Workout", "distance: " + w.getDistance());
                Log.i("Workout", "duration: " + w.getDuration());
                Log.i("Workout", "calories: " + w.getTotalCalories());
                Log.i("Workout", "pace: " + w.getPaceAvg());
                //Log.i("Workout", "duration: " + duration);

            }
        };

        Button activeMap = (Button) view.findViewById(R.id.button_stopwatch_activeworkout);
        activeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ActiveWorkoutMapActivity.class);
                i.putExtra("workoutId", workoutId);
                startActivity(i);
            }
        });

        final Button selectSport = (Button) view.findViewById(R.id.button_stopwatch_selectsport);
        selectSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                builderSingle.setTitle("Select Sport Activity");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.select_dialog_singlechoice);
                for (Integer activity : SportActivities.activities) {
                    arrayAdapter.add(SportActivities.getSportActivityName(activity));
                }

                builderSingle.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sportActivity = which;
                        if (sportActivity == SportActivities.RUNNING) {
                            selectSport.setText("Running");
                        } else if (sportActivity == SportActivities.WALKING) {
                            selectSport.setText("Walking");
                        } else {
                            selectSport.setText("Cycling");
                        }

                        Intent i = new Intent(getActivity(), TrackerService.class);
                        i.setAction(updateActivity);
                        i.putExtra("sportActivity", sportActivity);
                        getActivity().startService(i);
                    }
                });
                builderSingle.show();
            }
        });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionsNotGranted = false;
                }
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK"));
        Intent intent = new Intent(getActivity(), TrackerService.class);
        getActivity().startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        if (state == STATE_STOPPED || state == STATE_PAUSED) {
            Intent intent = new Intent(getActivity(), TrackerService.class);
            getActivity().stopService(intent);
        }
    }
}
