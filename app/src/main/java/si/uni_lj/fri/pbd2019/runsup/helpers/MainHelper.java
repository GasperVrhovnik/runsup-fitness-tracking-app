package si.uni_lj.fri.pbd2019.runsup.helpers;

public final class MainHelper {

    /* constants */
    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;

    /**
     * return string of time in format HH:MM:SS
     * @param time - in seconds
     */
    public static String formatDuration(long time) {
        long hours = time / 3600;
        long secondsLeft = time - hours * 3600;

        long minutes = secondsLeft / 60;

        long seconds = secondsLeft - minutes * 60;

        String format = "";
        if (hours < 10) {
            format += "0";
        }
        format += hours + ":";

        if (minutes < 10) {
            format += "0";
        }
        format += minutes + ":";

        if (seconds < 10) {
            format += "0";
        }
        format += seconds;

        return format;
    }

    public static String formatDuration2(long time) {
        long hours = time / 3600;
        long secondsLeft = time - hours * 3600;

        long minutes = secondsLeft / 60;

        long seconds = secondsLeft - minutes * 60;

        String format = "";
        format += hours + ":";

        if (minutes < 10) {
            format += "0";
        }
        format += minutes + ":";

        if (seconds < 10) {
            format += "0";
        }
        format += seconds;

        return format;
    }

    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    public static String formatDistance(double n) {
        double km = n / 1000;
        km = Math.round(km * 100.0) / 100.0;
        return Double.toString(km);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    public static String formatPace(double n) {
        double m = Math.round(n * 100.0) / 100.0;
        return Double.toString(m);
    }

    /**
     * round number to integer
     */
    public static String formatCalories(double n) {
        return Integer.toString((int) Math.round(n));
    }

    /* convert km to mi (multiply with a corresponding constant) */
    public static double kmToMi(double n) {
        return  n * KM_TO_MI;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n) {
        return n * MpS_TO_MIpH;
    }

    /* convert min/km to min/mi (multiply with a corresponding constant) */
    public static double minpkmToMinpmi(double n) {
        return n * MINpKM_TO_MINpMI;
    }



}
