package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;


import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;


public class ActiveWorkoutMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private DatabaseHelper databaseHelper;

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    private Marker marker;
    private Long workoutId;
    private Handler mHandler = new Handler();
    private  Runnable runnable;
    private Workout workout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_activeworkoutmap_map);
        mapFragment.getMapAsync(this);

        mFusedLocationProviderClient = new FusedLocationProviderClient(this);
        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        Intent i = getIntent();
        workoutId = i.getLongExtra("workoutId", 0);



        Button backBtn = (Button) findViewById(R.id.button_activeworkoutmap_back);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveWorkoutMapActivity.this.onBackPressed();
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        getLocations();

//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Dao<GpsPoint,Long> gpsDao = databaseHelper.gpsPointDao();

                    List<GpsPoint> gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", 0).query();
                    boolean vsajEnPoint = gpsList.size() > 0;

                    if (vsajEnPoint) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude()))).setTitle("Start");


                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                    }
                    GpsPoint last = null;
                    if (vsajEnPoint) {
                        last = gpsList.get(gpsList.size() - 1);
                    }
                    int i = 1;
                    while(gpsList.size() > 0) {
                        gpsList = gpsDao.queryBuilder().where().eq("workout_id", workoutId).and().eq("sessionNumber", i).query();
                        if (gpsList.size() <= 0) {
                            break;
                        }
                        Location lastL = new Location(LocationManager.GPS_PROVIDER);
                        lastL.setLatitude(last.getLatitude());
                        lastL.setLongitude(last.getLongitude());
                        Location firstL = new Location(LocationManager.GPS_PROVIDER);
                        firstL.setLatitude(gpsList.get(0).getLatitude());
                        firstL.setLongitude(gpsList.get(0).getLongitude());
                        if (lastL.distanceTo(firstL) > 100) {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(gpsList.get(0).getLatitude(), gpsList.get(0).getLongitude())).title("Continue  " + i)).showInfoWindow(); //first
                        } else {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("Break  " + i)).showInfoWindow();
                        }

                        last = gpsList.get(gpsList.size() - 1);
                        PolylineOptions pOptions = new PolylineOptions();
                        for (GpsPoint point : gpsList) {
                            pOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
                        }
                        mMap.addPolyline(pOptions);

                        i++;
                    }
                    try {
                        Dao<Workout, Long> workoutLongDao = databaseHelper.workoutDao();
                        workout = workoutLongDao.queryForId(workoutId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if (workout.getStatus() == Workout.statusEnded) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(last.getLatitude(), last.getLongitude())).title("End")).showInfoWindow();
                    }



                } catch (SQLException e) {
                    e.printStackTrace();
                }
                mHandler.postDelayed(this, 15000);
            }
        };

        runnable.run();







    }


    public void getLocations() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(15000);
        locationRequest.setFastestInterval(15000);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location l = locationResult.getLastLocation();
                LatLng latLng = new LatLng(l.getLatitude(), l.getLongitude());
                moveToCurrentLocation(latLng);
            }
        };
        if(!(ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        }
    }

    private void moveToCurrentLocation(LatLng currentLocation)
    {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 17));
        // Zoom in, animating the camera.
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mHandler.removeCallbacks(runnable);
    }
}
