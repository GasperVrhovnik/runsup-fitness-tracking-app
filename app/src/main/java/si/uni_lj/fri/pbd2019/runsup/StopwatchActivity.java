package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;



public class StopwatchActivity extends AppCompatActivity {

    public final String startCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public final String contCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public final String pauseCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public final String stopCmd = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    private final Integer RUNNING = 0;
    private final Integer WALKING = 1;
    private final Integer CYCLING = 2;
    public static final int STATE_RUNNING = 0;
    public static final int STATE_STOPPED = 1;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_CONTINUE = 3;

    private int state;


    private BroadcastReceiver mBroadcastReceiver;
    private int locationRequestCode = 1000;

    private Button start;
    private Button endWorkout;
    private Button btnSportActivity;

    private TextView distanceText;
    private TextView durationText;
    private TextView paceText;
    private TextView caloriesText;

    private ArrayList<List<Location>> positionList;
    private LinkedList<Location> currPositionList;
    private Integer sportActivity;
    private long duration;
    private double distance;
    private double pace;
    private double calories;

    private boolean workout;
    private boolean permissionsNotGranted;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);


        distanceText = (TextView) findViewById(R.id.textview_stopwatch_distance);
        durationText = (TextView) findViewById(R.id.textview_stopwatch_duration);
        paceText = (TextView) findViewById(R.id.textview_stopwatch_pace);
        caloriesText = (TextView) findViewById(R.id.textview_stopwatch_calories);

        btnSportActivity = (Button) findViewById(R.id.button_stopwatch_selectsport);
        start = (Button) findViewById(R.id.button_stopwatch_start);
        endWorkout = (Button) findViewById(R.id.button_stopwatch_endworkout);

        positionList = new ArrayList<List<Location>>();
        sportActivity = RUNNING;
        duration = 0;
        distance = 0.0;
        pace = 0.0;

        workout = false;

        permissionsNotGranted = ActivityCompat.checkSelfPermission(StopwatchActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(StopwatchActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;

        if (permissionsNotGranted) {
            ActivityCompat.requestPermissions(StopwatchActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
        }
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (start.getText().toString().equals(getResources().getString(R.string.stopwatch_start))) {
                    if(ActivityCompat.checkSelfPermission(StopwatchActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(StopwatchActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(StopwatchActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
                    } else {
                        workout = true;
                        state = STATE_RUNNING;
                    }
                } else if (start.getText().toString().equals(getResources().getString(R.string.stopwatch_stop))) {
                    state = STATE_PAUSED;
                } else {
                    state = STATE_CONTINUE;
                }

                Intent i = new Intent(StopwatchActivity.this, TrackerService.class);
                switch (state) {
                    case STATE_RUNNING:
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        i.setAction(startCmd);
                        break;
                    case STATE_CONTINUE:
                        start.setText(getResources().getString(R.string.stopwatch_stop));
                        endWorkout.setVisibility(View.GONE);
                        i.setAction(contCmd);
                        break;
                    case STATE_PAUSED:
                        start.setText(getResources().getString(R.string.stopwatch_continue));
                        endWorkout.setVisibility(View.VISIBLE);
                        positionList.add(currPositionList); //dodaj novo pot po pauzi
                        currPositionList = new LinkedList<Location>(); //nastavi prazno pot za morebiten continue
                        i.setAction(pauseCmd);
                        break;
                    case STATE_STOPPED:
                        i.setAction(stopCmd);
                        break;
                }
                Log.i("service", i.getAction());
                startService(i);


            }
        });





        endWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(StopwatchActivity.this, WorkoutDetailActivity.class);
//                startActivity(intent);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(StopwatchActivity.this);
                builder1.setMessage("Do you really want to end workout and reset counters?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                workout = false;
                                state = STATE_STOPPED;
                                Intent intent = new Intent(StopwatchActivity.this, WorkoutDetailActivity.class);
                                intent.putExtra("sportActivity", sportActivity);
                                intent.putExtra("duration", duration);
                                intent.putExtra("distance", distance);
                                intent.putExtra("calories", calories);
                                intent.putExtra("finalPositionList", positionList);
                                pace = ((double) duration / 60.0) / (distance / 1000.0);
                                intent.putExtra("pace", pace);
                                startActivity(intent);
                            }
                        });

                builder1.setNegativeButton(
                        getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                distance = intent.getExtras().getDouble("distance");
                duration = intent.getExtras().getLong("duration");
                calories = intent.getExtras().getDouble("calories");
                ArrayList<Location> currPositionArray = (ArrayList<Location>) intent.getExtras().get("positionList");
                currPositionList = new LinkedList<Location>();
                for (Location l : currPositionArray) {
                    currPositionList.add(l);
                }

                String distanceString = MainHelper.formatDistance(distance);
                String durationString = MainHelper.formatDuration(duration);
                String paceString = MainHelper.formatPace(intent.getExtras().getDouble("pace"));
                String caloriesString = MainHelper.formatCalories(calories);

                distanceText.setText(distanceString);
                durationText.setText(durationString);
                paceText.setText(paceString);
                caloriesText.setText(caloriesString);
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK"));
        Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
        startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        if (!workout) {
            Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
            stopService(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
        stopService(intent);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start.setText(getResources().getString(R.string.stopwatch_stop));
                    workout = true;
                    state = STATE_RUNNING;
                    Intent i = new Intent(StopwatchActivity.this, TrackerService.class);
                    i.setAction(startCmd);
                    startService(i);
                }
                break;
            }
        }
    }
}
