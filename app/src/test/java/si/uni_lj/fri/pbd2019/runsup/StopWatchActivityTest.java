package si.uni_lj.fri.pbd2019.runsup;

import android.content.ComponentName;
import android.content.Intent;
import android.widget.Button;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowApplication;

import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.robolectric.Shadows.shadowOf;
//import static si.uni_lj.fri.pbd2019.runsup.helpers.UnitTestHelper.forgetAllNextStartedServices;
//import static si.uni_lj.fri.pbd2019.runsup.helpers.UnitTestHelper.forgetAllNextStoppedServices;
//import static si.uni_lj.fri.pbd2019.runsup.helpers.UnitTestHelper.resetSingleton;

@RunWith(RobolectricTestRunner.class)
//@Config(constants = BuildConfig.class)
public final class StopWatchActivityTest {

    private Button startButton;
    private Button endWorkoutButton;
    private TrackerService service;

    private StopwatchActivity activity;

    @Before
    public void setUp() {
//        ShadowLog.stream = System.out;
        service = new TrackerService();
        ShadowApplication shapp = ShadowApplication.getInstance();
        shapp.setComponentNameAndServiceForBindService(ComponentName.createRelative("", "TrackerService"), service.onBind(new Intent()));
        activity = Robolectric.buildActivity(StopwatchActivity.class)
                .create()
                .resume()
                .get();
        startButton = (Button) activity.findViewById(R.id.button_stopwatch_start);
        endWorkoutButton = (Button) activity.findViewById(R.id.button_stopwatch_endworkout);
    }

//    @After
//    public void finishComponentTesting() {
//        resetSingleton(OpenHelperManager.class, "helper");
//    }

    @Test
    public void clickContinueShouldStartTrackerServiceWithContinueAction() {
        shadowOf(activity).getNextStartedService(); // clear intents started from StopwatchActivity

        Intent expectedIntent = new Intent(activity, TrackerService.class); // expected service
        expectedIntent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE"); // expected action

        startButton.performClick(); // click start
        shadowOf(activity).getNextStartedService();
        startButton.performClick(); // click stop
        shadowOf(activity).getNextStartedService();
        //forgetAllNextStartedServices(activity); // clear all previous started services
        startButton.performClick(); // click continue

        Intent receivedIntent = shadowOf(activity).getNextStartedService(); // find next service

        assertNotNull("No service started after clicking Continue."+receivedIntent.toString(), receivedIntent);
        assertTrue("Expected starting TrackerService.class with action COMMAND_CONTINUE but other service started. Started service"+receivedIntent.toString(), receivedIntent.filterEquals(expectedIntent));
    }


}